
PROGRAM _INIT
	
	MapHwLimits_0.Axis := ADR(gAxis_1);
	MapHwLimits_0.Execute := TRUE;
	MapHwLimits_0.ModuleAddress := 'SL1.IF1.ST10.IF1.ST4';
	MapHwLimits_0.ChannelName := 'DigitalInput05';
	 
END_PROGRAM

PROGRAM _CYCLIC
	
	MapHwLimits_0( );
	
END_PROGRAM

PROGRAM _EXIT
	 
END_PROGRAM

