#include <powerlnk.h>
#include <McAcpAx.h>
#include <HwLimits.h>

#include "acp10par.h"
#include <bur/plctypes.h>

void MapHwLimits(MapHwLimits_typ *p) 
{
	static UINT bitOffset;
	
	switch (p->step)
	{
		case 0:
			
			if (p->Execute)
			{
				p->Busy = 1;
				p->step++;
			}
			break;

		case 1:
			
			p->crosslinkInfo.DeviceName = p->ModuleAddress;
			p->crosslinkInfo.ChannelName = p->ChannelName;
			
			UINT status = plAction(0, plACTION_GET_CROSSLINK_INFO, &p->crosslinkInfo, sizeof(p->crosslinkInfo));
			if (status == 0)
			{
				p->step++;
			}
			else
			{
				p->ErrorID = status;
				p->step = 100;
			}
			break;
		
		case 2:
			{
				bitOffset = 32 * (p->crosslinkInfo.Offset/32);	
				p->parIds[0].ParID = ACP10PAR_CYCLIC_DP_DATA_OFFSET;
				p->parIds[0].VariableAddress = (UDINT)&bitOffset;
				p->parIds[0].DataType = mcACPAX_PARTYPE_UINT;
	
				static UINT UI4 = 7;	
				p->parIds[1].ParID = ACP10PAR_CYCLIC_DP_DATA_TYPE;
				p->parIds[1].VariableAddress = (UDINT)&UI4;
				p->parIds[1].DataType = mcACPAX_PARTYPE_UINT;

				static ConfigCyclicPos_typ configCyclicPos;
				configCyclicPos.nodeNumber = p->crosslinkInfo.NodeId;	
				configCyclicPos.parID = ACP10PAR_CYCLIC_DP_DATA_OFFSET;
				p->parIds[2].ParID = ACP10PAR_CONFIG_MA1_CYCLIC_POS ;
				p->parIds[2].VariableAddress = (UDINT)&configCyclicPos;
				p->parIds[2].DataType = mcACPAX_PARTYPE_VOID;

				static USINT off = 0;	
				p->parIds[3].ParID = ACP10PAR_MA1_CYCLIC_IPL_MODE;
				p->parIds[3].VariableAddress = (UDINT)&off;
				p->parIds[3].DataType = mcACPAX_PARTYPE_USINT;

				static UINT BIT_MODE = ACP10PAR_BIT_MODE;//42005;
				p->parIds[4].ParID = ACP10PAR_FUNCTION_BLOCK_CREATE;
				p->parIds[4].VariableAddress = (UDINT)&BIT_MODE;
				p->parIds[4].DataType = mcACPAX_PARTYPE_UINT;

				static UDINT oneBit = 1;	
				p->parIds[5].ParID = ACP10PAR_BIT_A1;
				p->parIds[5].VariableAddress = (UDINT)&oneBit;
				p->parIds[5].DataType = mcACPAX_PARTYPE_UDINT;

				p->parIds[6].ParID = ACP10PAR_BIT_A2;
				p->parIds[6].VariableAddress = (UDINT)&oneBit;
				p->parIds[6].DataType = mcACPAX_PARTYPE_UDINT;

				p->parIds[7].ParID = ACP10PAR_BIT_A3;
				p->parIds[7].VariableAddress = (UDINT)&oneBit;
				p->parIds[7].DataType = mcACPAX_PARTYPE_UDINT;

				static UDINT shift1;
				shift1 = p->crosslinkInfo.Offset%32;	
				p->parIds[8].ParID = ACP10PAR_BIT_B1;
				p->parIds[8].VariableAddress = (UDINT)&shift1;
				p->parIds[8].DataType = mcACPAX_PARTYPE_UDINT;

				static UDINT shift2;
				shift2 = p->crosslinkInfo.Offset%32 + 1;	
				p->parIds[9].ParID = ACP10PAR_BIT_B2;
				p->parIds[9].VariableAddress = (UDINT)&shift2;
				p->parIds[9].DataType = mcACPAX_PARTYPE_UDINT;

				static UDINT shift3;
				shift3 = p->crosslinkInfo.Offset%32 + 2;	
				p->parIds[10].ParID = ACP10PAR_BIT_B3;
				p->parIds[10].VariableAddress = (UDINT)&shift3;
				p->parIds[10].DataType = mcACPAX_PARTYPE_UDINT;

				static UINT ma1CyclicPos = ACP10PAR_MA1_CYCLIC_POS;	
				p->parIds[11].ParID = ACP10PAR_BIT_IN1_PARID;
				p->parIds[11].VariableAddress = (UDINT)&ma1CyclicPos;
				p->parIds[11].DataType = mcACPAX_PARTYPE_UINT;

				p->parIds[12].ParID = ACP10PAR_BIT_IN2_PARID;
				p->parIds[12].VariableAddress = (UDINT)&ma1CyclicPos;
				p->parIds[12].DataType = mcACPAX_PARTYPE_UINT;

				p->parIds[13].ParID = ACP10PAR_BIT_IN3_PARID;
				p->parIds[13].VariableAddress = (UDINT)&ma1CyclicPos;
				p->parIds[13].DataType = mcACPAX_PARTYPE_UINT;

				static UINT ten = 10;	
				p->parIds[14].ParID = ACP10PAR_BIT_MODE;
				p->parIds[14].VariableAddress = (UDINT)&ten;
				p->parIds[14].DataType = mcACPAX_PARTYPE_UINT;

				static UINT BIT_VALUE1 = ACP10PAR_BIT_VALUE1;	
				p->parIds[15].ParID = ACP10PAR_POS_LIMIT_SWITCH_PARID;
				p->parIds[15].VariableAddress = (UDINT)&BIT_VALUE1;
				p->parIds[15].DataType = mcACPAX_PARTYPE_UINT;

				static UINT BIT_VALUE2 = ACP10PAR_BIT_VALUE2;	
				p->parIds[16].ParID = ACP10PAR_NEG_LIMIT_SWITCH_PARID;
				p->parIds[16].VariableAddress = (UDINT)&BIT_VALUE2;
				p->parIds[16].DataType = mcACPAX_PARTYPE_UINT;

				static UINT BIT_VALUE3 = ACP10PAR_BIT_VALUE3;	
				p->parIds[17].ParID = ACP10PAR_REFERENCE_SWITCH_PARID;
				p->parIds[17].VariableAddress = (UDINT)&BIT_VALUE3;
				p->parIds[17].DataType = mcACPAX_PARTYPE_UINT;
				
				p->step++;
				break;
			}
			
		case 3:
			
			p->MC_BR_ProcessParID_AcpAx_0.Axis = p->Axis;
			p->MC_BR_ProcessParID_AcpAx_0.Execute = 1;
			p->MC_BR_ProcessParID_AcpAx_0.DataAddress = (UDINT)p->parIds;
			p->MC_BR_ProcessParID_AcpAx_0.NumberOfParIDs = 18;
			p->MC_BR_ProcessParID_AcpAx_0.Mode = mcACPAX_PARID_SET;
			MC_BR_ProcessParID_AcpAx(&p->MC_BR_ProcessParID_AcpAx_0);
	
			if (p->MC_BR_ProcessParID_AcpAx_0.Done)
			{
				p->MC_BR_ProcessParID_AcpAx_0.Execute = 0;
				MC_BR_ProcessParID_AcpAx(&p->MC_BR_ProcessParID_AcpAx_0);
				p->step++;
			}
			
			if (p->MC_BR_ProcessParID_AcpAx_0.Error)
			{			
				p->ErrorID = p->MC_BR_ProcessParID_AcpAx_0.ErrorID;
				p->MC_BR_ProcessParID_AcpAx_0.Execute = 0;
				MC_BR_ProcessParID_AcpAx(&p->MC_BR_ProcessParID_AcpAx_0);
				p->step = 100;
			}
			break;
			
		case 4:
			
			p->Done = 1;
			p->Busy = 0;
			
			if (p->Execute == 0)
			{
				p->Done = 0;
				p->step = 0;
			}
			break;
		
		case 100:
			
			p->Error = 1;
			p->Busy = 0;

			if (p->Execute == 0)
			{
				p->Error = 0;
				p->ErrorID = 0;
				p->step = 0;
			}
			break;
		
	}
     
	
	
}




