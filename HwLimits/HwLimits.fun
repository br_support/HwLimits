
FUNCTION_BLOCK MapHwLimits
	VAR_INPUT
		Axis : REFERENCE TO McAxisType;
		Execute : BOOL;
		ModuleAddress : STRING[80];
		ChannelName : STRING[80];
	END_VAR
	VAR_OUTPUT
		Done : BOOL;
		Busy : BOOL;
		Error : BOOL;
		ErrorID : DINT;
	END_VAR
	VAR
		step : INT;
		crosslinkInfo : PLACTION_GET_CROSSLINK_INFO_typ;
		MC_BR_ProcessParID_AcpAx_0 : MC_BR_ProcessParID_AcpAx;
		parIds : ARRAY[0..19] OF McAcpAxProcessParIDType;
	END_VAR
END_FUNCTION_BLOCK
